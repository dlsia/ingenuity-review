def binarySearch(lst, n, left, right):
    mid = int((left + right)/2)
    if n == lst[mid]:
        return mid
    elif n > lst[mid]:
        return binarySearch(lst, n, mid+1, right)
    elif n < lst[mid]:
        return binarySearch(lst, n, left, mid-1)
    else:
        return -1

n = binarySearch([1,2,3,6,8,9], 9, 0, 5)
print(n)
