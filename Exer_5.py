import os
lst = []
file_path = 'exer_5_data.txt'
if not os.path.exists(file_path):
    #print('hey')
    open(file_path, 'a').close()

with open('exer_5_data.txt', 'r') as reader:
    for line in reader:
        lst += [line.replace('\n', ' ')]

print('Enter string 1:', end=' ')
lst += [input()]
print('Enter string 2:', end=' ')
lst += [input()]
print('Enter string 3:', end=' ')
lst += [input()]
lst.sort()
[print(x) for x in lst]

with open('exer_5_data.txt', 'w') as writer:
    for x in lst:
        writer.write(x + '\n')
