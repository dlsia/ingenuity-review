def create_multiplication_table(c, r=None):
    if r is None:
        r = c
    c_list = [x for x in range(1, c+1)]

    table = []
    for r_val in range(1, r+1):
        row = [r_val * c_val for c_val in range(1, c+1)]
        table += [row]
    return table

t = create_multiplication_table(5,4)

for r in t:
    for c in r:
        print('%d\t' %c, end=' ')
    print()
