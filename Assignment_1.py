SCOREBOARD = {}

from random import randint
import datetime
import time

#If user just press enter instead of username, the program displays Top 10 users with the highest scores.
#Sorting is based first on time.
#If two or more users have the same time, sorting will now be based on number of errors.
#The Top 10 display should contain the username, time, and errors, all sorted out.
def print_scoreboard():
    print('Rank\tName\tTime\tErrors')
    sorted_list = sorted(SCOREBOARD.items(), key=lambda x: (x[1][0], x[1][1]))
    for i, (name, (time, errors)) in enumerate(sorted_list):
        total_secs = time.total_seconds()
        print('[%d]\t%s\t%02d:%02d\t%d' % (i+1, name, total_secs/60, total_secs % 60, errors))


#Program will generate 10 random arithmetic problems based on these guidelines:
#Addition of 2-digit with 2-digit
#Subtraction of 2-digit with 2-digit (make sure answer is not negative)
#Multiplication of 2-digit with 1-digit (“1” is not included)
#Division of 3-digit with 1-digit (make sure they are divisible) (“1” is not included)
def generate_problem():
    x = randint(0, 3)
    if x == 0:
        problem = [randint(10, 99), '+', randint(10, 99)]
    elif x == 1: #Subtraction
        num1 = randint(10, 99)
        num2 = randint(10, num1)
        problem = [num1, '-', num2]
    elif x == 2: #Multiplication
        problem = [randint(10, 99), '*', randint(2, 9)]
    elif x == 3: #Division
        num1 = randint(100, 999)
        num2 = randint(2, 9)
        num1 = num1 - (num1 % num2)
        problem = [num1, '/', num2]
    problem = ' '.join(str(x) for x in problem)
    return problem
    
#Program asks user for username
#If user enters username, a new game begins.
#If the username is already existing, check if time is faster. If yes, overwrite their score with the new score.

while True:
    print('Please input username', end=' ')
    username = input()
    if username == '':
        print_scoreboard()
        continue

    #Program asks user to begin game
    print('Begin game? (Y/N)')
    choice = input()
    if choice == 'Y' or choice == 'y':
        print("Timer starts now!")
    elif choice == 'N' or choice == 'n':
        print("Alright, let's not play")
    else:
        print("Invalid choice")


    #User will have to answer each problem one-by-one.
    #They have to enter the number, then press enter.
    #If the answer is correct, they get one point.
    #If not, they will get one error, and will have to answer another problem.
    SCORE = 0
    ERROR = 0

    start = time.time()
    while SCORE is not 10 and ERROR is not 10:
        problem = generate_problem()
        print(problem)
        answer = int(float(input()))
        if answer == eval(problem):
            SCORE += 1
            print('Right!')
        else:
            ERROR += 1
            print('Wrong!')

    end = time.time()

    #Game ends when user already has 10 points.
    #Game also ends when user already has 10 errors (but will not record the score). This denotes that user fails the challenge.
    #Once the game is over, program will display user's score
    #Time it took to finish the game (in mm:ss) - round off to nearest second
    #Number of errors
    if SCORE is 10:
        
        time_taken = round(end-start)
        days = int(time_taken/86400)
        secs = int(time_taken%86400)
        time_taken = datetime.timedelta(days=days, seconds=secs)
        total_secs = time_taken.total_seconds()
        print('Congratulations! You solved 10 problems!')
        print('Time taken: %02d:%02d' % (total_secs/60, total_secs%60))
        print('Number of errors:', ERROR)
        SCOREBOARD[username] = (time_taken, ERROR)
        #Save the score in a dictionary, with keys username, time, and errors.
        #Program then asks for user to input another username
        
        if username in SCOREBOARD and time_taken > SCOREBOARD[username][0]:
            SCOREBOARD[username][0] = time_taken
    else:
        print('Sorry, you already have 10 errors. Score will not be recorded')
