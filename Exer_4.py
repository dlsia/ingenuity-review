import re
print('Enter string:')
line = input().lower()

exp = r'[^A-Za-z\s]*'

line = re.sub(exp, '', line)

lst = line.split(' ')

dict = {}
for x in lst:
    if x not in dict:
        dict[x] = 1
    else:
        dict[x] += 1
print(dict)
