class BankAccount(object):
    interest_rate = 0.3

    def __init__(self, name, number, balance):
        self.name = name
        self.number = number
        self.balance = balance

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        self.balance -= amount

    def add_interest(self):
        self.balance += interest_rate * self.balance

class StudentAccount(BankAccount):
    overdraft_limit = 1000

    def __init__(self, name, number, balance):
        super().__init__(name, number, balance)

    def withdraw(self, amount):
        if self.balance - amount < (StudentAccount.overdraft_limit * -1):
            print('Cannot process. You will exceed your overdraft limit. ')
        else:
            self.balance -= amount


student = StudentAccount('Jon', '0101', 500) #balance = 500
student.deposit(100) #balance = 600
student.withdraw(300) #balance = 300
student.withdraw(300) #balance = 0
student.withdraw(1) #balance = -1
student.withdraw(999) #balance = -1000. Overdraft limit reached.
print(student.balance)
