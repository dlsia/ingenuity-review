def recursive_sort(lst, i=0):
    if i == len(lst) - 1:
        return lst
    for j, x in enumerate(lst):
        if j == len(lst) - 1:
            break
        if lst[j] > lst[j+1]:
            lst[j], lst[j+1] = lst[j+1], lst[j]

    i +=1
    return recursive_sort(lst, i) 
print(recursive_sort([1,4, 6,2], 0))
