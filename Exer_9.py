def timing_decorator(orig_func):
    import time
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        print('Time it took to run the function: %0.3f ms' % t2)
        return result
    return wrapper

@timing_decorator
def adding_function(x, y):
    print('The sum is {}.'.format(x+y))

adding_function(3,5)
