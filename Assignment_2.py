from random import randint
import time
import csv
from tabulate import tabulate

class Score:
    def __init__(self, points=0, errors=0, time=0):
        self.points = points
        self.errors = errors
        self.time = time

    def addPoint(self):
        self.points += 1
        
    def addError(self):
        self.errors += 1
        
    def addTime(self,time):
        self.time += round(time)
        
    def getFormattedTime(self):
        mins = int(self.time / 60)
        secs = self.time % 60
        s = "%02d:%02d" % (mins, secs)
        return s

    def game_done(self):
        return self.points is 3 or self.errors is 3

class User:
    def __init__(self, name,points=0, errors=0, time=0):
        self.name = name
        self.score = Score(points, errors, time)
    def __str__(self):
        s = "%s\t%d\t%s" % (self.name, self.score.errors, self.score.getFormattedTime())
        return s

    def __eq__(self, other):
        return self.name == other.name
        
    

class Records:
    def __init__(self):
        self.records = []
        
    def add_record(self, user):
        self.records += [user]
        
    def update_record(self, user):
        for record in self.records:
            #print(self.records)
            if user.name == record.name:
                record.score.errors = user.score.errors
                record.score.time = user.score.time
                
    def sort_records(self):
        self.records.sort(key=lambda user: (user.score.time, user.score.errors))
        
    def read_records(self, filename='highscores.csv'):
        with open(filename, 'r') as file:
            reader = csv.reader(file, delimiter=',')
            next(reader) #Skip header
            for row in reader:
                name, points, errors, time = row
                user = User(name, int(points), int(errors), int(time))
                self.records += [user]
    
    def write_records(self, filename='highscores.csv'):
            with open(filename, 'w') as file:
                writer = csv.writer(file, delimiter=',')
                writer.writerow(['name', 'points', 'errors', 'time'])
                for row in self.records:
                    writer.writerow([row.name, row.score.points, row.score.errors, row.score.time])

    def __str__(self):
        self.sort_records()
        header = ['Rank', 'Name', 'Errors', 'Time']
        body = []
        for i,record in enumerate(self.records):
            if i == 10:
                break
            body += [[i+1, record.name, record.score.errors, record.score.getFormattedTime()]]
        table = tabulate(body, headers=header)
        return '[SCOREBOARD]\n' +table

    def user_exists(self, user):
        return user in self.records

    def is_faster(self, user):
        assert user in self.records, "User is not in the records."

        for u in self.records:
            if user == u and user.score.time < u.score.time:
                return True
        return False


class Problem:

    @staticmethod
    def generate_addition():
        problem = [randint(10, 99), '+', randint(10, 99)]
        return problem

    @staticmethod
    def generate_subtraction():
        num1 = randint(10, 99)
        num2 = randint(10, num1)
        problem = [num1, '-', num2]
        return problem

    @staticmethod
    def generate_multiplication():
        problem = [randint(10, 99), '*', randint(2, 9)]
        return problem

    @staticmethod
    def generate_division():
        num1 = randint(100, 999)
        num2 = randint(2, 9)
        num1 = num1 - (num1 % num2)
        problem = [num1, '/', num2]
        return problem

    @staticmethod
    def generate_problem():
        problem_type = randint(0, 3)

        if problem_type == 0:
            problem = Problem.generate_addition()
        elif problem_type == 1:
            problem = Problem.generate_subtraction()
        elif problem_type == 2:
            problem = Problem.generate_multiplication()
        else:
            problem = Problem.generate_division()
            
        problem = ' '.join(str(x) for x in problem)
        return problem

    @staticmethod
    def check_answer(answer, problem):
        return answer == eval(problem) 
    


records = Records()
records.read_records()
while True:
    print('Please input username:', end=' ') #Program asks user for username
    username = input() #If user enters username, a new game begins.
    if username == '':
        print(records)
        continue

    user = User(username)
    #Program asks user to begin game
    print('Begin game? (Y/N)')
    choice = input()
    if choice == 'Y' or choice == 'y':
        print("Timer starts now!")
    elif choice == 'N' or choice == 'n':
        print("Alright, let's not play")
        break
    else:
        print("Invalid choice")
        continue

    
    start = time.time()
    while not user.score.game_done():
        problem = Problem.generate_problem()
        print(problem)
        print(eval(problem))
        answer = int(float(input()))

        if Problem.check_answer(answer, problem):
            user.score.addPoint()
            print('Right!')
        else:
            user.score.addError()
            print('Wrong!')
    end = time.time()

    
    
    if user.score.points is 3: #Game ends when user already has 10 points.
        
        user.score.addTime(end-start)
        print('Congratulations! You solved 10 problems!') #Once the game is over, program will display user's score
        print('Time taken: %s' % user.score.getFormattedTime()) #Time it took to finish the game (in mm:ss) - round off to nearest second
        print('Number of errors:', user.score.errors) #Number of errors
        
        if records.user_exists(user): #If the username is already existing, check if time is faster. If yes, overwrite their score with the new score.
            if records.is_faster(user):
                records.update_record(user)
        else:
            records.add_record(user)
    else: #Game also ends when user already has 10 errors (but will not record the score). This denotes that user fails the challenge.
        print('Sorry, you already have 10 errors. Score will not be recorded')
    
    records.write_records()